% Usage

# Usage

The director performs actions within the workflow based upon events from the
hosting service and commands contained within comments in the appropriate
places.

Comments containing commands must have their commands at their end. They follow
Git's trailer `Key: value` format. When a comment with a command is found and
handled, the comment is marked by the robot as handled using an emoji such as
:robot: or similar depending on the support of the hosting service. Commands
which are successful may be configured to not create a new comment in reply in
order to help keep the noise in the discussion to a minimum.

Commands are acted upon based on permissions of the user within the project and
the required levels may differ between projects. Refer to project documentation
for information related to the required permissions for the commands.

Commands may also accept arguments to tweak their behavior. Arguments do not
support quoting and are split on whitespace.

Some commands may not be combined with each other. For example, it does not
make sense to request a `stage` and `unstage` action together or a `stage` and
a `merge` action. An error will be posted back in reply if incompatible
commands are given.

Not all commands may be used by a project. For example, a project which does
not have a stage branch will not have the `stage` or `unstage` command
available. Refer to project documentation for the supported commands.

## Commands

### `Do: check`

Check the merge request for errors based on the contents and history.

  * *Permissions*: anyone
  * *Arguments*: none

By default, this action is performed on any applicable update to a merge
request itself (such as updating the source branch, opening or reopening the
merge request, changing the target branch, and others depending on the hosting
service).

Merge requests may contain errors that are not suitable for a project. This
command checks every commit in a merge request's topic for errors. Note that
due to the way the checks work, fixup commits will not hide errors from the
checks; instead, the commits causing errors or warnings must be fixed using
either `git commit --amend` or `git rebase`. There are multiple tutorials
available online for how to rebase a topic. Here are a few:

  * https://git-scm.com/book/en/v2/Git-Branching-Rebasing
  * https://www.atlassian.com/git/tutorials/rewriting-history

Note that the warnings about rewriting history are not as applicable to a
fork-based workflow because other developers do not typically use forks
directly. However, if a merge request is under development by multiple people,
coordination with history manipulation is recommended.

### `Do: reformat`

Reformat a branch with reformatting tools.

  * *Permissions*: usually required; submitter is allowed
  * *Arguments*: none

Projects may enforce style guidelines using a variety of reformatting tools.
These tools may not be widely available or easily accessible for all
contributors, so a topic may be reformatted using the tool by the `reformat`
command. Every commit in the merge request's topic is rewritten using the tool
and then is pushed back to the source repository once it is complete.

Not all formatting tools will rewrite source code, so its availability is
subject to tool support. See project documentation for limitations.

### `Do: stage` and `Do: unstage`

Change the state of a merge request on the target branch's "stage" branch.

  * *Permissions*: usually required; submitter is allowed to `unstage`
  * *Arguments*: none

The stage branch is a collection of merges from staged merge requests into the
target branch. It's typical use case is to handle integration testing of
multiple topics at once for testing with testing infrastructure which may not
be able to keep up with the regular update pace of a project in real time.

The stage is recreated when the target branch updates for any reason and any
branches which fail to merge are removed from the staging branch.

### `Do: test`

Submit the merge request for testing.

  * *Permissions*: usually required
  * *Arguments*: see below

The test action may be handled by different backends based on the project.
Refer to the project documentation for which backend is in use for it. The
arguments for the `test` action depends on the backend in use for the project.

#### `refs` Backend

For the `refs` backend, no arguments are supported.

#### `jobs` Backend

For the `jobs` backend, arguments are split and passed on as part of the job
file. No checking is done as it is considered part of the test job handler to
interpret the arguments. See documentation for the project for supported
arguments.

### `Do: merge`

Merge the merge request into the target branch.

  * *Permissions*: usually required
  * *Arguments*:
    - `--topic <name>` (or `-t`): use a different name for the topic when
      merging.

The description of the merge request may also contain the following trailer "commands":

  - `Topic-rename`: renames the source branch as far as the robot is concerned
    for merging purposes (namely the commit message).
  - `Backport`: Takes two possible forms:
    - `branchname`: also merge this MR topic to the named branch
    - `branchname:commit`: where `commit` may use `HEAD` to refer to the main
      commit of the merge request; mainly used as something like `HEAD~3` or
      similar to handle conflict resolution with a single merge request.
  - `Fast-forward`: Only accepts the `true` argument. When provided, the
    primary branch target will be merged using `--ff-only`. Typically used to
    update release branches so that tags live directly on branches instead of
    coming in from topic commits.

The merge action merges a topic from a merge request into the target branch as
well as any specified backport branches using the given topic name and pushes
all new branch heads to the target project. Information for the merge commit
message is gathered from the description of and comments on the merge request.

Note that branches may be configured to "sync" with other branches. For
example, a `release` branch may be configured to do so for `master`. When this
is done, any merge into `release` will also perform a merge using `-s ours` to
"sync" `release` into `master` so that `release` is always reachable from
`master`. When backports are used, all direct branch merges are performed
before these "sync" merges.

Comments since the last update which include the following are gathered and
added to the set of information added to the commit message:

  - Trailers ending in `-by` support user references and the `me` value to
    perform user lookup and replacement.
  - A comment may have a line starting with any of the following markers:
    - `+1` or `:+1:` indicating `Acked-by`;
    - `+2` indicating `Reviewed-by`;
    - `+3` indicating `Tested-by`; or
    - `-1` or `:-1:` indicating `Rejected-by`.
  - The merge request itself may be noted with an emoji (the skin tone
    modified variants are also accepted):
    - :100:, :clap:, or :tada: indicating `Acked-by`;
    - :no_good: indicating `Rejected-by`.

#### Merge policies

The default policy currently discards any trailer with a key not ending in
`-by`.

Merges may be configured to require certain CI states from "stages" in the CI
pipeline. For example, builds have to be green, but tests just need to finish.
Or everything must be green.

Projects may also be set up with a "bypass token" which allows ignoring other
policy failure reasons and merging anyways. Note that a failing `Do: check`
cannot be bypassed using this mechanism. A bypass token is a cryptographic hash
of information including the MR URL, commit hash, and a shared secret.
