// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Actions to be performed on projects.
//!
//! This module contains actions which may be performed on various entities on a project.
//! They are grouped by the entity on which they perform their effects.

use chrono::{DateTime, Utc};
use ghostflow::host::User;
use serde_json::Value;

use crate::config::Project;
use crate::ghostflow_ext::AccessLevel;

/// The cause of an action.
pub struct Cause {
    /// When the action was triggered.
    pub when: DateTime<Utc>,
    /// Who requested the action.
    pub who: User,
    /// The effective access level of the cause for the project in question.
    pub how: AccessLevel,
    /// Whether the requester of the action owns the entity for the action.
    pub is_submitter: bool,
}

/// Common data for actions.
pub struct Data<'a> {
    /// The project on which the action is being performed.
    pub project: &'a Project,
    /// A blob of data which may be used.
    pub job_data: &'a Value,
    /// The cause of the action.
    pub cause: Cause,
}

mod utils;

pub mod merge_requests;
