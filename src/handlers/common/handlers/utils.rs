// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use ghostflow::host::HostingServiceError;
use json_job_dispatch::JobResult;
use log::{error, info};
use serde_json::{json, Value};
use thiserror::Error;

use crate::config::{Branch, Host, Project, ProjectStatus};

macro_rules! try_action {
    ( $action:expr ) => {
        match $action {
            Ok(res) => res,
            Err(handler_result) => return handler_result,
        }
    };
}

#[derive(Debug, Error)]
pub enum ProjectError {
    #[error("uninitialized project: {}", project)]
    UninitializedProject { project: String },
}

struct ProjectResult;

impl ProjectResult {
    fn uninitialized_project(project: String) -> JobResult {
        JobResult::fail(ProjectError::UninitializedProject {
            project,
        })
    }

    fn unwatched_project(project: &str) -> JobResult {
        JobResult::reject(format!("unwatched project {}", project))
    }
}

/// Get the project from a host.
pub fn get_project<'a>(host: &'a Host, project_name: &str) -> Result<&'a Project, JobResult> {
    match host.project(project_name) {
        ProjectStatus::Initialized(project) => Ok(project),
        ProjectStatus::FailedToInitialize => {
            Err(ProjectResult::uninitialized_project(project_name.into()))
        },
        ProjectStatus::Unknown => Err(ProjectResult::unwatched_project(project_name)),
    }
}

struct BranchResult;

impl BranchResult {
    fn unwatched_branch(project: &str, branch: &str) -> JobResult {
        JobResult::reject(format!(
            "unwatched branch {} for project {}",
            branch, project,
        ))
    }
}

/// Get the project and branch from a host.
pub fn get_branch<'a>(
    host: &'a Host,
    project_name: &str,
    branch_name: &str,
) -> Result<(&'a Project, &'a Branch), JobResult> {
    get_project(host, project_name).and_then(|project| {
        project
            .branches
            .get(branch_name)
            .map(|branch| (project, branch))
            .ok_or_else(|| BranchResult::unwatched_branch(project_name, branch_name))
    })
}

/// Handle a job result and commit if errors occurred.
pub fn handle_result<F>(
    messages: &[String],
    errors: &[String],
    defer: bool,
    post_comment: F,
) -> JobResult
where
    F: Fn(&str) -> Result<(), HostingServiceError>,
{
    let txt_comment = format!(
        "messages:\n{}\n\nerrors:\n{}",
        messages.join("\n"),
        errors.join("\n"),
    );

    let (comment, result) = if defer {
        info!(
            target: "ghostflow-director/handler",
            "Deferring a job: {}",
            txt_comment,
        );

        (None, JobResult::defer(txt_comment))
    } else if errors.is_empty() {
        let md_comment = if messages.is_empty() {
            None
        } else {
            Some(format!("Messages:\n\n  - {}", messages.join("\n  - ")))
        };

        (md_comment, JobResult::Accept)
    } else {
        let md_comment = if messages.is_empty() {
            format!("Errors:\n\n  - {}", errors.join("\n  - "))
        } else {
            format!(
                "Messages:\n\n  - {}\n\nErrors: \n\n  - {}",
                messages.join("\n  - "),
                errors.join("\n  - "),
            )
        };

        (Some(md_comment), JobResult::reject(txt_comment))
    };

    if let Some(comment) = comment {
        if let Err(err) = post_comment(&comment) {
            error!(
                target: "ghostflow-director/handler",
                "Failed to post comment:\n'{}'\n'{:?}'.",
                comment, err,
            );
        }
    }

    result
}

/// Create a test job.
pub fn test_job<K>(kind: K, data: &Value) -> Value
where
    K: AsRef<str>,
{
    json!({
        "kind": kind.as_ref(),
        "data": data,
    })
}
