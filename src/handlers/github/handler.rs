// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::cmp::Ordering;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::io;
use std::sync::{Arc, RwLock};

use async_trait::async_trait;
use either::Either;
use ghostflow::host::HostingServiceError;
use ghostflow_github::{Github, GithubService};
use itertools::Itertools;
use json_job_dispatch::{Director, Handler, HandlerCore, JobResult};
use log::{info, warn};
use serde::de::DeserializeOwned;
use serde::Deserialize;
use serde_json::Value;
use thiserror::Error;

use crate::config::Host;
use crate::handlers::common::handlers::*;
use crate::handlers::common::jobs::{
    BatchBranchJob, ClearTestRefs, ResetFailedProjects, TagStage, UpdateFollowRefs,
};
use crate::handlers::github::hooks::*;
use crate::handlers::github::traits::*;
use crate::handlers::HostHandler;

#[derive(Debug, Error)]
pub enum GithubHostError {
    #[error("failed to deserialize GitHub secrets for host {}: {}", host, source)]
    DeserializeSecrets {
        host: String,
        #[source]
        source: serde_json::Error,
    },
    #[error(
        "failed to read the private key `{}` for host {}: {}",
        path,
        host,
        source
    )]
    ReadPrivateKey {
        host: String,
        path: String,
        #[source]
        source: io::Error,
    },
    #[error("failed to construct client for host {}: {}", host, source)]
    ConstructClient {
        host: String,
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
    #[error("failed to construct service for host {}: {}", host, source)]
    ConstructService {
        host: String,
        #[source]
        source: HostingServiceError,
    },
}

impl GithubHostError {
    fn deserialize_secrets(host: String, source: serde_json::Error) -> Self {
        GithubHostError::DeserializeSecrets {
            host,
            source,
        }
    }

    fn read_private_key(host: String, path: String, source: io::Error) -> Self {
        GithubHostError::ReadPrivateKey {
            host,
            path,
            source,
        }
    }

    fn construct_client(host: String, source: Box<dyn Error + Send + Sync>) -> Self {
        GithubHostError::ConstructClient {
            host,
            source,
        }
    }

    fn construct_service(host: String, source: HostingServiceError) -> Self {
        GithubHostError::ConstructService {
            host,
            source,
        }
    }
}

#[derive(Debug, Deserialize)]
enum GithubSecrets {
    #[serde(rename = "app")]
    App {
        app_id: i64,
        private_key_path: String,
        installation_ids: HashMap<String, i64>,
    },
}

/// Connect to a Github from its configuration block.
pub fn host_handler(
    url: &Option<String>,
    secrets: &Value,
    name: String,
) -> Result<HostHandler, GithubHostError> {
    let host = url.as_ref().map(AsRef::as_ref).unwrap_or("api.github.com");

    if host != "api.github.com" {
        warn!(
            target: "github",
            "Enterprise deployments are not known to work ({})",
            host,
        );
    }

    let secrets: GithubSecrets = serde_json::from_value(secrets.clone())
        .map_err(|err| GithubHostError::deserialize_secrets(host.into(), err))?;
    let github = match secrets {
        GithubSecrets::App {
            app_id,
            private_key_path,
            installation_ids,
        } => {
            let private_key = fs::read(&private_key_path).map_err(|err| {
                GithubHostError::read_private_key(host.into(), private_key_path, err)
            })?;
            Github::new_app(host, app_id, private_key, installation_ids)
                .map_err(|err| GithubHostError::construct_client(host.into(), err.into()))?
        },
    };

    let service = GithubService::new(github)
        .map(Arc::new)
        .map_err(|err| GithubHostError::construct_service(host.into(), err))?;

    Ok(HostHandler {
        service: service.clone(),
        handler: Box::new(|host| {
            Box::new(GithubHandler::new(host, service, name)) as Box<dyn Handler>
        }),
    })
}

/// The handler for Github events.
struct GithubHandler {
    /// The host block for this handler.
    host: Arc<RwLock<Host>>,
    /// The specific `GithubService` for this connection.
    ///
    /// This is required because we need to make our own query due to a lack of information in
    /// webhooks.
    github: Arc<GithubService>,
    /// The name to use for this handler.
    name: String,
}

const HOST_LOCK_POISONED: &str = "host lock poisoned";

impl GithubHandler {
    /// Create a new handler.
    fn new(host: Host, github: Arc<GithubService>, name: String) -> Self {
        GithubHandler {
            host: Arc::new(RwLock::new(host)),
            github,
            name,
        }
    }

    /// Verify that the kind is valid.
    fn verify_kind<'a>(&self, kind: &'a str) -> Either<&'a str, JobResult> {
        let mut split = kind.split(':');

        if let Some(level) = split.next() {
            if level != self.name {
                return Either::Right(JobResult::reject(format!("handler mismatch: {}", level)));
            }
        } else {
            return Either::Right(JobResult::reject("handler mismatch"));
        }

        if let Some(kind) = split.next() {
            Either::Left(kind)
        } else {
            Either::Right(JobResult::reject("missing kind"))
        }
    }

    /// Parse an object into a type.
    fn parse_object<F, T>(object: &Value, callback: F) -> JobResult
    where
        T: DeserializeOwned,
        F: Fn(T) -> JobResult,
    {
        match serde_json::from_value::<T>(object.clone()) {
            Ok(hook) => callback(hook),
            Err(err) => JobResult::fail(err),
        }
    }

    /// Handle a job.
    fn handle_kind(
        host: Arc<RwLock<Host>>,
        github: Arc<GithubService>,
        kind: String,
        object: Value,
        can_defer: bool,
    ) -> JobResult {
        match kind.as_ref() {
            "check_run" => {
                Self::parse_object(&object, |hook: CheckRunEvent| {
                    info!(
                        target: "github",
                        "check run {} {:?} on {}",
                        hook.check_run.name,
                        hook.action,
                        hook.pull_requests
                            .map(|prs| {
                                format!(
                                    "{}",
                                    prs.iter()
                                        .map(|pr| {
                                            let repo_name = pr.base.repo.as_ref().map_or("unknown", |r| r.full_name.as_ref());
                                            format!("{}#{}", repo_name, pr.number)
                                        })
                                        .format(", "),
                                )
                            })
                            .unwrap_or_else(|| "<no PRs?>".into()),
                    );

                    JobResult::Accept
                })
            },
            "check_suite" => {
                Self::parse_object(&object, |hook: CheckSuiteEvent| {
                    info!(
                        target: "github",
                        "check suite {:?}",
                        // "check suite {:?} on {}",
                        hook.action,
                        // hook.check_suite.pull_requests.iter().map(|pr| format!("{}#{}", pr.base.repo.full_name, pr.number)).format(", "),
                    );

                    JobResult::Accept
                })
            },
            "github_app_authorization" => {
                Self::parse_object(&object, |hook: GithubAppAuthorizationEvent| {
                    info!(
                        target: "github",
                        "application authorization {:?}",
                        hook.action,
                    );

                    JobResult::Accept
                })
            },
            "installation" => {
                Self::parse_object(&object, |hook: InstallationEvent| {
                    info!(
                        target: "github",
                        "installation hook {:?}: {} by {}",
                        hook.action,
                        hook.installation.id,
                        hook.installation.account.login,
                    );

                    JobResult::Accept
                })
            },
            "installation_repositories" => {
                Self::parse_object(&object, |hook: InstallationRepositoriesEvent| {
                    info!(
                        target: "github",
                        "installation hook {:?} for {:?} repositories: {} by {}: {}{}",
                        hook.action,
                        hook.repository_selection,
                        hook.installation.id,
                        hook.installation.account.login,
                        hook.repositories_added.iter().map(|repo| &repo.full_name).format(" +"),
                        hook.repositories_removed.iter().map(|repo| &repo.full_name).format(" -"),
                    );

                    JobResult::Accept
                })
            },
            "issue_comment" => {
                Self::parse_object(&object, |hook: IssueCommentEvent| {
                    GithubMergeRequestNoteInfo::from_web_hook(github.as_ref(), &hook)
                        .map(|note| {
                            if let Some(note) = note {
                                let host = host.read().expect(HOST_LOCK_POISONED);
                                handle_merge_request_note(&object, &host, &note, can_defer)
                            } else {
                                JobResult::reject(format!(
                                    "comment on non-pr: {}#{}",
                                    hook.repository.full_name, hook.issue.number,
                                ))
                            }
                        })
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "member" => {
                Self::parse_object(&object, |hook: MemberEvent| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_project_membership_refresh(&object, &host, &hook.repository.full_name)
                })
            },
            "membership" => {
                Self::parse_object(&object, |hook: MembershipEvent| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_group_membership_refresh(&object, &host, &hook.organization.login)
                })
            },
            "organization" => {
                Self::parse_object(&object, |hook: OrganizationEvent| {
                    if let OrganizationAction::MemberInvited = hook.action {
                        return JobResult::Accept;
                    }
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_group_membership_refresh(&object, &host, &hook.organization.login)
                })
            },
            "pull_request_review" => {
                Self::parse_object(&object, |hook: PullRequestReviewEvent| {
                    GithubMergeRequestNoteInfo::from_web_hook_review(github.as_ref(), &hook)
                        .map(|note_res| {
                            match note_res {
                                Ok(note) => {
                                    let host = host.read().expect(HOST_LOCK_POISONED);
                                    handle_merge_request_note(&object, &host, &note, can_defer)
                                },
                                Err(err) => {
                                    if let GithubMergeRequestError::Desync {
                                        ..
                                    } = err
                                    {
                                        JobResult::defer(err.to_string())
                                    } else {
                                        JobResult::fail(err)
                                    }
                                },
                            }
                        })
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "pull_request" => {
                Self::parse_object(&object, |hook: PullRequestEvent| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    GithubMergeRequestInfo::from_web_hook(host.service.as_ref(), &hook)
                        .map(|mr_res| {
                            match mr_res {
                                Ok(mr) => handle_merge_request_update(&object, &host, &mr),
                                Err(err) => {
                                    if let GithubMergeRequestError::Desync {
                                        ..
                                    } = err
                                    {
                                        JobResult::defer(err.to_string())
                                    } else {
                                        JobResult::fail(err)
                                    }
                                },
                            }
                        })
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "push" => {
                Self::parse_object(&object, |hook: PushEvent| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    GithubPushInfo::from_web_hook(host.service.as_ref(), hook)
                        .map(|push| handle_push(&object, &host, &push))
                        .unwrap_or_else(JobResult::fail)
                })
            },
            "team" => {
                Self::parse_object(&object, |hook: TeamEvent| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    if let Some(ref repo) = hook.repository {
                        handle_project_membership_refresh(&object, &host, &repo.full_name)
                    } else if let Some(ref org) = hook.organization {
                        handle_group_membership_refresh(&object, &host, &org.login)
                    } else {
                        JobResult::reject("no associated repository or organization")
                    }
                })
            },
            "team_add" => {
                Self::parse_object(&object, |hook: TeamAddEvent| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_project_membership_refresh(&object, &host, &hook.repository.full_name)
                })
            },
            "clear_test_refs" => {
                Self::parse_object(&object, |data: BatchBranchJob<ClearTestRefs>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_clear_test_refs(&object, &host, data)
                })
            },
            "tag_stage" => {
                Self::parse_object(&object, |data: BatchBranchJob<TagStage>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_stage_tag(&object, &host, data)
                })
            },
            "update_follow_refs" => {
                Self::parse_object(&object, |data: BatchBranchJob<UpdateFollowRefs>| {
                    let host = host.read().expect(HOST_LOCK_POISONED);
                    handle_update_follow_refs(&object, &host, data)
                })
            },
            "reset_failed_projects" => {
                Self::parse_object(&object, |_: ResetFailedProjects| {
                    host.write()
                        .expect(HOST_LOCK_POISONED)
                        .reset_failed_projects();
                    JobResult::Accept
                })
            },
            _ => JobResult::reject(format!("unhandled kind: {}", kind)),
        }
    }
}

impl HandlerCore for GithubHandler {
    fn add_to_director<'a>(
        &'a self,
        director: &mut Director<'a>,
    ) -> Result<(), Box<dyn Error + Send + Sync>> {
        let mut add_handler = |kind| director.add_handler(format!("{}:{}", self.name, kind), self);

        add_handler("check_run")?;
        add_handler("check_suite")?;
        add_handler("github_app_authorization")?;
        add_handler("installation")?;
        add_handler("installation_repositories")?;
        add_handler("issue_comment")?;
        add_handler("member")?;
        add_handler("membership")?;
        add_handler("organization")?;
        add_handler("pull_request_review")?;
        add_handler("pull_request")?;
        add_handler("push")?;
        add_handler("team")?;
        add_handler("team_add")?;

        add_handler("clear_test_refs")?;
        add_handler("tag_stage")?;
        add_handler("update_follow_refs")?;

        add_handler("reset_failed_projects")?;

        Ok(())
    }
}

const DEFAULT_RETRY_LIMIT: usize = 5;

#[async_trait]
impl Handler for GithubHandler {
    async fn handle(
        &self,
        kind: &str,
        object: &Value,
        count: usize,
    ) -> Result<JobResult, Box<dyn Error + Send + Sync>> {
        let can_defer = match count.cmp(&DEFAULT_RETRY_LIMIT) {
            Ordering::Greater => {
                return Ok(JobResult::reject(format!(
                    "retry limit ({}) reached for {}",
                    count, kind,
                )))
            },
            Ordering::Equal => false,
            Ordering::Less => true,
        };

        let kind = match self.verify_kind(kind) {
            Either::Left(kind) => kind,
            Either::Right(res) => return Ok(res),
        };

        let host = self.host.clone();
        let github = self.github.clone();
        let kind = kind.into();
        let object = object.clone();

        tokio::task::spawn_blocking(move || {
            Self::handle_kind(host, github, kind, object, can_defer)
        })
        .await
        .map_err(|err| Box::new(err) as Box<dyn Error + Send + Sync>)
    }
}
