// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Configuration management data structures.

mod checks;
pub(crate) mod defaults;
pub mod io;
mod merge_policy;
mod types;

pub use self::io::Config as ConfigRead;
pub use self::io::TestPipelinesActor;
pub use self::types::*;

#[cfg(test)]
pub use self::checks::Checks;
